﻿using MyPlanesLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Авиакомпания.Airline;

namespace Авиакомпания
{
    class Program
    {
        static void Main(string[] args)
        {
            Airline airline = AirlineFactory.CreateAirline();

            PassangerCalculator Calculator = new PassangerCalculator();

            int totalAmount = Calculator.CalculatePassangers(airline);

            Console.WriteLine("Total passanger capacity: " + totalAmount);
            Console.WriteLine();

            ShowPlanes show = new ShowPlanes();
            show.Show(airline);
            Console.ReadKey();
        }
    }
    public class ShowPlanes
    {
        public void Show(Airline airline)
        {
            List<PlaneCommonInfo> planes = airline.GetPlanePassangers();
            Console.WriteLine("The company's aircrafts: ");
            Console.WriteLine();
            foreach (PlaneCommonInfo plane in planes)
            {
                Console.WriteLine("Name: " + plane.name);
                Console.WriteLine("Number of pasangers:" + plane.passangers);
                Console.WriteLine();
            }
        }
    }

}
