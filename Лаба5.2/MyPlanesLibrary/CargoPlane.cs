﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Авиакомпания
{
    public class CargoPlane : Plane
    {
        private int _CarryingCapacity;

        public int GetCarryingCapacity()
        {
            return _CarryingCapacity;
        }
        public void SetCarryingCapacity(int _CarryingCapacity)
        {
            this._CarryingCapacity = _CarryingCapacity;
        }
    }
}

