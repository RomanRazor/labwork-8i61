﻿using System;
using System.Collections.Generic;
using System.Text;
using Авиакомпания;

namespace MyPlanesLibrary
{
    public class AirlineFactory
    {
        public static Airline CreateAirline()
        {
            PassengerPlane PLANE1 = new PassengerPlane();
            PLANE1.SetCruisingSpeed(800);
            PLANE1.SetModel("ТУ-154");
            PLANE1.SetMaxDistance(5000);
            PLANE1.SetMaxHight(11000);
            PLANE1.SetPassengerCapacity(140);
            PLANE1.SetWeight(55300);
            PLANE1.SetSpetialFIeld("Ближнемагистральный");

            PlaneCommonInfo plane1;
            plane1.name = PLANE1.GetModel();
            plane1.passangers = PLANE1.GetPassengerCapacity();

            CargoPlane PLANE2 = new CargoPlane();
            PLANE2.SetCruisingSpeed(700);
            PLANE2.SetModel("Boeing 777");
            PLANE2.SetMaxDistance(4000);
            PLANE2.SetMaxHight(11500);
            PLANE2.SetSpetialFIeld("7000");
            PLANE2.SetWeight(75300);

            PlaneCommonInfo plane2;
            plane2.name = PLANE2.GetModel();
            plane2.passangers = PLANE2.GetPassengerCapacity();

            PassengerPlane PLANE3 = new PassengerPlane();
            PLANE3.SetCruisingSpeed(900);
            PLANE3.SetModel("Airbus А330");
            PLANE3.SetMaxDistance(7500);
            PLANE3.SetMaxHight(12500);
            PLANE3.SetPassengerCapacity(175);
            PLANE3.SetWeight(47600);
            PLANE3.SetPassengerCapacity(200);
            PLANE3.SetSpetialFIeld("Дальнемагистральный");

            PlaneCommonInfo plane3;
            plane3.name = PLANE3.GetModel();
            plane3.passangers = PLANE3.GetPassengerCapacity();

            CargoPlane PLANE4 = new CargoPlane();
            PLANE4.SetCruisingSpeed(830);
            PLANE4.SetModel("Airbus A320");
            PLANE4.SetMaxDistance(4000);
            PLANE4.SetMaxHight(12200);
            PLANE4.SetSpetialFIeld("5500");
            PLANE4.SetWeight(52700);

            PlaneCommonInfo plane4;
            plane4.name = PLANE4.GetModel();
            plane4.passangers = PLANE4.GetPassengerCapacity();

            Airline airline = new Airline();
            airline.addPlane(PLANE1, plane1);
            airline.addPlane(PLANE2, plane2);
            airline.addPlane(PLANE3, plane3);
            airline.addPlane(PLANE4, plane4);

            return airline;
        }
    }
}
