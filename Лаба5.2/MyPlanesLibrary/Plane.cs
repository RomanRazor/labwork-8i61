﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Авиакомпания
{
    public class Plane
    {
        private string _model;
        private double _weight;
        private double _MaxHight;
        private int _СruisingSpeed;
        private int _MaxDistance;
        private int _PassengerCapacity;
        private string _SpetialField;

        public string GetModel()
        {
            return _model;
        }
        public void SetModel(string _model)
        {
            this._model = _model;
        }
        public string GetSpetialField()
        {
            return _SpetialField;
        }
        public void SetSpetialFIeld(string _SpetialField)
        {
            this._SpetialField = _SpetialField;
        }
        public double GetWeight()
        {
            return _weight;
        }
        public void SetWeight(double _weight)
        {
            this._weight = _weight;
        }
        public double GetMaxHight()
        {
            return _MaxHight;
        }
        public void SetMaxHight(double _MaxHight)
        {
            this._MaxHight = _MaxHight;
        }
        public int GetCruisingSpeed()
        {
            return _СruisingSpeed;
        }
        public void SetCruisingSpeed(int _СruisingSpeed)
        {
            this._СruisingSpeed = _СruisingSpeed;
        }
        public int GetMaxDistance()
        {
            return _MaxDistance;
        }
        public void SetMaxDistance(int _MaxDistance)
        {
            this._MaxDistance = _MaxDistance;
        }
        public int GetPassengerCapacity()
        {
            return _PassengerCapacity;
        }
        public void SetPassengerCapacity(int _PassengerCapacity)
        {
            this._PassengerCapacity = _PassengerCapacity;
        }
    }
}

