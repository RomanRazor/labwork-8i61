﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Авиакомпания
{
    public class PassengerPlane : Plane
    {
        private string _TypeOfPlane;

        public string GetTypeOfPlane()
        {
            return _TypeOfPlane;
        }
        public void SetTypeOfPlane(string _TypeOfPlane)
        {
            this._TypeOfPlane = _TypeOfPlane;
        }
    }
}
