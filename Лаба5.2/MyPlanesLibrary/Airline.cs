﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Авиакомпания
{
    public struct PlaneCommonInfo
    {
        public int passangers;
        public string name;

        public PlaneCommonInfo(int p1, string p2)
        {
            passangers = p1;
            name = p2;
        }
        public override string ToString()
        {
            return  name + " - " + passangers.ToString();
        }
        //public static implicit operator PlaneCommonInfo(Plane v)
        //{
        //    throw new NotImplementedException();
        //}
    }
    public class Airline
    {
        private List<Plane> planes;
        private List<PlaneCommonInfo> pp;

        public void EditPlane(Plane plane, int i)
        {
            if (planes[i].GetCruisingSpeed() != plane.GetCruisingSpeed())
                planes[i].SetCruisingSpeed(plane.GetCruisingSpeed());
            if (planes[i].GetMaxDistance() != plane.GetMaxDistance())
                planes[i].SetMaxDistance(plane.GetMaxDistance());
            if (planes[i].GetMaxHight() != plane.GetMaxHight())
                planes[i].SetMaxHight(plane.GetMaxHight());
            if (planes[i].GetModel() != plane.GetModel())
                planes[i].SetModel(plane.GetModel());
            if (planes[i].GetPassengerCapacity() != plane.GetPassengerCapacity())
                planes[i].SetPassengerCapacity(plane.GetPassengerCapacity());
            if (planes[i].GetWeight() != plane.GetWeight())
                planes[i].SetWeight(plane.GetWeight());
            if (planes[i].GetSpetialField() != plane.GetSpetialField().ToString())
                planes[i].SetSpetialFIeld(plane.GetSpetialField().ToString());
        }
        public void EditPlaneCommonInfo(PlaneCommonInfo plane, int i)
        {
            if (pp[i].name != plane.name)
            {
                PlaneCommonInfo p1 = pp[i];
                p1.name = planes[i].GetModel();
                pp[i] = p1;
            }
            if (pp[i].passangers != plane.passangers)
            {
                PlaneCommonInfo p1 = pp[i];
                p1.passangers = planes[i].GetPassengerCapacity();
                pp[i] = p1;
            }
        }
        public void deletePlane(Plane plane, PlaneCommonInfo planeCommonInfo)
        {
            planes.Remove(plane);
            pp.Remove(planeCommonInfo);
        }
        public void addPlane(Plane plane, PlaneCommonInfo planeCommonInfo)
        {
            if (planes == null)
                planes = new List<Plane>();
            planes.Add(plane);

            if (pp == null)
                pp = new List<PlaneCommonInfo>();
            pp.Add(planeCommonInfo);

        }
        public List<Plane> GetPlanes()
        {
            return planes;
        }
        public List<PlaneCommonInfo> GetPlanePassangers()
        {
            return pp;
        }
        public void SetPlanes(List<Plane> planes)
        {
            this.planes = planes;
        }
        public class PassangerCalculator
        {
            public int CalculatePassangers(Airline airline)
            {
                int totalAmount = 0;

                List<PlaneCommonInfo> planes = airline.GetPlanePassangers();

                foreach (PlaneCommonInfo plane in planes)
                {
                    totalAmount = totalAmount + plane.passangers;
                }
                return totalAmount;
            }
        }
    }
}