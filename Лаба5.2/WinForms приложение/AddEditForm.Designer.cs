﻿namespace WinForms_приложение
{
    partial class AddEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSpetialField = new System.Windows.Forms.TextBox();
            this.labelSpetialField = new System.Windows.Forms.Label();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.textBoxPassangerCapacity = new System.Windows.Forms.TextBox();
            this.labelPassangerCapacity = new System.Windows.Forms.Label();
            this.textBoxWeight = new System.Windows.Forms.TextBox();
            this.labelWeight = new System.Windows.Forms.Label();
            this.textBoxMaxDistance = new System.Windows.Forms.TextBox();
            this.labelMaxDistance = new System.Windows.Forms.Label();
            this.textBoxCruisingSpeed = new System.Windows.Forms.TextBox();
            this.labelCruisingSpeed = new System.Windows.Forms.Label();
            this.textBoxMaxHight = new System.Windows.Forms.TextBox();
            this.labelMaxHight = new System.Windows.Forms.Label();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.labelModel = new System.Windows.Forms.Label();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.labelClass = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxSpetialField
            // 
            this.textBoxSpetialField.Location = new System.Drawing.Point(182, 204);
            this.textBoxSpetialField.Name = "textBoxSpetialField";
            this.textBoxSpetialField.Size = new System.Drawing.Size(123, 20);
            this.textBoxSpetialField.TabIndex = 37;
            // 
            // labelSpetialField
            // 
            this.labelSpetialField.AutoSize = true;
            this.labelSpetialField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSpetialField.Location = new System.Drawing.Point(44, 208);
            this.labelSpetialField.Name = "labelSpetialField";
            this.labelSpetialField.Size = new System.Drawing.Size(94, 16);
            this.labelSpetialField.TabIndex = 36;
            this.labelSpetialField.Text = "Type of plane:";
            // 
            // comboBox
            // 
            this.comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Location = new System.Drawing.Point(182, 16);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(123, 21);
            this.comboBox.TabIndex = 35;
            this.comboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox_SelectedIndexChanged_1);
            // 
            // textBoxPassangerCapacity
            // 
            this.textBoxPassangerCapacity.Location = new System.Drawing.Point(182, 178);
            this.textBoxPassangerCapacity.Name = "textBoxPassangerCapacity";
            this.textBoxPassangerCapacity.Size = new System.Drawing.Size(123, 20);
            this.textBoxPassangerCapacity.TabIndex = 34;
            // 
            // labelPassangerCapacity
            // 
            this.labelPassangerCapacity.AutoSize = true;
            this.labelPassangerCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPassangerCapacity.Location = new System.Drawing.Point(44, 182);
            this.labelPassangerCapacity.Name = "labelPassangerCapacity";
            this.labelPassangerCapacity.Size = new System.Drawing.Size(131, 16);
            this.labelPassangerCapacity.TabIndex = 33;
            this.labelPassangerCapacity.Text = "Passanger сapacity:";
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Location = new System.Drawing.Point(182, 152);
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.Size = new System.Drawing.Size(123, 20);
            this.textBoxWeight.TabIndex = 32;
            // 
            // labelWeight
            // 
            this.labelWeight.AutoSize = true;
            this.labelWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWeight.Location = new System.Drawing.Point(44, 156);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(53, 16);
            this.labelWeight.TabIndex = 31;
            this.labelWeight.Text = "Weigth:";
            // 
            // textBoxMaxDistance
            // 
            this.textBoxMaxDistance.Location = new System.Drawing.Point(182, 126);
            this.textBoxMaxDistance.Name = "textBoxMaxDistance";
            this.textBoxMaxDistance.Size = new System.Drawing.Size(123, 20);
            this.textBoxMaxDistance.TabIndex = 30;
            // 
            // labelMaxDistance
            // 
            this.labelMaxDistance.AutoSize = true;
            this.labelMaxDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMaxDistance.Location = new System.Drawing.Point(44, 130);
            this.labelMaxDistance.Name = "labelMaxDistance";
            this.labelMaxDistance.Size = new System.Drawing.Size(90, 16);
            this.labelMaxDistance.TabIndex = 29;
            this.labelMaxDistance.Text = "Max distance:";
            // 
            // textBoxCruisingSpeed
            // 
            this.textBoxCruisingSpeed.Location = new System.Drawing.Point(182, 100);
            this.textBoxCruisingSpeed.Name = "textBoxCruisingSpeed";
            this.textBoxCruisingSpeed.Size = new System.Drawing.Size(123, 20);
            this.textBoxCruisingSpeed.TabIndex = 28;
            // 
            // labelCruisingSpeed
            // 
            this.labelCruisingSpeed.AutoSize = true;
            this.labelCruisingSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCruisingSpeed.Location = new System.Drawing.Point(44, 104);
            this.labelCruisingSpeed.Name = "labelCruisingSpeed";
            this.labelCruisingSpeed.Size = new System.Drawing.Size(101, 16);
            this.labelCruisingSpeed.TabIndex = 27;
            this.labelCruisingSpeed.Text = "Cruising speed:";
            // 
            // textBoxMaxHight
            // 
            this.textBoxMaxHight.Location = new System.Drawing.Point(182, 74);
            this.textBoxMaxHight.Name = "textBoxMaxHight";
            this.textBoxMaxHight.Size = new System.Drawing.Size(123, 20);
            this.textBoxMaxHight.TabIndex = 26;
            // 
            // labelMaxHight
            // 
            this.labelMaxHight.AutoSize = true;
            this.labelMaxHight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMaxHight.Location = new System.Drawing.Point(44, 78);
            this.labelMaxHight.Name = "labelMaxHight";
            this.labelMaxHight.Size = new System.Drawing.Size(67, 16);
            this.labelMaxHight.TabIndex = 25;
            this.labelMaxHight.Text = "Max hight:";
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(182, 48);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(123, 20);
            this.textBoxModel.TabIndex = 24;
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelModel.Location = new System.Drawing.Point(44, 52);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(49, 16);
            this.labelModel.TabIndex = 23;
            this.labelModel.Text = "Model:";
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonCancel.Location = new System.Drawing.Point(182, 247);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 33);
            this.ButtonCancel.TabIndex = 22;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // OkButton
            // 
            this.OkButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OkButton.Location = new System.Drawing.Point(84, 247);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 33);
            this.OkButton.TabIndex = 21;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // labelClass
            // 
            this.labelClass.AutoSize = true;
            this.labelClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelClass.Location = new System.Drawing.Point(44, 26);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(45, 16);
            this.labelClass.TabIndex = 20;
            this.labelClass.Text = "Class:";
            // 
            // AddEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 296);
            this.Controls.Add(this.textBoxSpetialField);
            this.Controls.Add(this.labelSpetialField);
            this.Controls.Add(this.comboBox);
            this.Controls.Add(this.textBoxPassangerCapacity);
            this.Controls.Add(this.labelPassangerCapacity);
            this.Controls.Add(this.textBoxWeight);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.textBoxMaxDistance);
            this.Controls.Add(this.labelMaxDistance);
            this.Controls.Add(this.textBoxCruisingSpeed);
            this.Controls.Add(this.labelCruisingSpeed);
            this.Controls.Add(this.textBoxMaxHight);
            this.Controls.Add(this.labelMaxHight);
            this.Controls.Add(this.textBoxModel);
            this.Controls.Add(this.labelModel);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.labelClass);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddEditForm";
            this.Text = "AddEditForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSpetialField;
        private System.Windows.Forms.Label labelSpetialField;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.TextBox textBoxPassangerCapacity;
        private System.Windows.Forms.Label labelPassangerCapacity;
        private System.Windows.Forms.TextBox textBoxWeight;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.TextBox textBoxMaxDistance;
        private System.Windows.Forms.Label labelMaxDistance;
        private System.Windows.Forms.TextBox textBoxCruisingSpeed;
        private System.Windows.Forms.Label labelCruisingSpeed;
        private System.Windows.Forms.TextBox textBoxMaxHight;
        private System.Windows.Forms.Label labelMaxHight;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Label labelClass;
    }
}