﻿using MyPlanesLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Авиакомпания;
using static Авиакомпания.Airline;

namespace WinForms_приложение
{
    public partial class MainForm : Form
    {
        Airline airline = AirlineFactory.CreateAirline();

        public class ShowPlanes
        {
            public void Show(Airline airline, ListBox dataBox)
            {
                List<PlaneCommonInfo> planes = airline.GetPlanePassangers();
                foreach (PlaneCommonInfo plane in planes)
                {
                    dataBox.Items.Add(plane);
                }
            }
        }
        public MainForm()
        {
            InitializeComponent();
            PassangerCalculator Calculator = new PassangerCalculator();
            int totalAmount = Calculator.CalculatePassangers(airline);
            List<PlaneCommonInfo> newplanes = airline.GetPlanePassangers();
            TotalCapacity.Text = "Total passanger capacity: " + totalAmount;
            ShowPlanes show = new ShowPlanes();
            show.Show(airline, dataBox);
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            AddEditForm addEditForm = new AddEditForm();
            addEditForm.ShowDialog();
            addObject(addEditForm);
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (dataBox.SelectedItem == null)
                MessageBox.Show("Please, choose the object");
            else
            {
                int index = dataBox.SelectedIndex;
                List<Plane> newplanes = airline.GetPlanes();
                AddEditForm addEditForm = new AddEditForm(newplanes[index]);
                addEditForm.ShowDialog();
                updateObject(addEditForm, index);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (dataBox.SelectedItem == null)
                MessageBox.Show("Please, choose the object");
            else
            {
                int index = dataBox.SelectedIndex;
                List<Plane> planes1 = airline.GetPlanes();
                List<PlaneCommonInfo> planes2 = airline.GetPlanePassangers();
                airline.deletePlane(planes1[dataBox.SelectedIndex], planes2[dataBox.SelectedIndex]);
            }
            dataBox.Items.Clear();
            TotalCapacity.Text = null;
            PassangerCalculator Calculator = new PassangerCalculator();
            int totalAmount = Calculator.CalculatePassangers(airline);
            TotalCapacity.Text = "Total passanger capacity: " + totalAmount;
            ShowPlanes show = new ShowPlanes();
            show.Show(airline, dataBox);
        }
        public void addObject(AddEditForm AddEditForm)
        {
            if (AddEditForm.GetDialogResult() == DialogResult.OK)
            {
                Plane plane = AddEditForm.GetPlane();
                PlaneCommonInfo planeCommonInfo = AddEditForm.GetPP();
                airline.addPlane(plane, planeCommonInfo);

                this.dataBox.Items.Add(planeCommonInfo);
                PassangerCalculator Calculator = new PassangerCalculator();
                int totalAmount = Calculator.CalculatePassangers(airline);
                TotalCapacity.Text = "Total passanger capacity: " + totalAmount;
            }
        }
        public void updateObject(AddEditForm AddEditForm, int index)
        {
            if (AddEditForm.GetDialogResult() == DialogResult.OK)
            {
                Plane plane = AddEditForm.GetPlane();
                airline.EditPlane(plane, index);
                PlaneCommonInfo plane1 = AddEditForm.GetPP();
                airline.EditPlaneCommonInfo(plane1, index);
                dataBox.Items.Clear();

                ShowPlanes show = new ShowPlanes();
                show.Show(airline, dataBox);
                PassangerCalculator Calculator = new PassangerCalculator();
                int totalAmount = Calculator.CalculatePassangers(airline);
                TotalCapacity.Text = "Total passanger capacity: " + totalAmount;
            }
        }
    }
}
