﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Авиакомпания;

namespace WinForms_приложение
{
    public partial class AddEditForm : Form
    {
        private DialogResult _result;

        public DialogResult GetDialogResult()
        {
            return _result;
        }
        public void SetDialogResult(DialogResult _result)
        {
            this._result = _result;
        }
        private Plane _plane;

        public Plane GetPlane()
        {
            Plane _pasplane = new Plane();

            SetDataFromForm(_pasplane);
            _pasplane.SetSpetialFIeld(textBoxSpetialField.Text);
            return _pasplane;
        }
        public void SetPlane(Plane _plane)
        {
            this._plane = _plane;
        }

        private PlaneCommonInfo _pp;

        public PlaneCommonInfo GetPP()
        {
            _pp.name = textBoxModel.Text;
            _pp.passangers = Convert.ToInt32(textBoxPassangerCapacity.Text);
            return _pp;
        }
        public void SetPP(PlaneCommonInfo _pp)
        {
            this._pp = _pp;
        }

        public AddEditForm()
        {
            InitializeComponent();
            comboBox.Items.Add("Passanger");
            comboBox.Items.Add("Cargo");
        }

        public AddEditForm(Plane plane) : this()
        {
            comboBox.Items.Add("Passanger");
            comboBox.Items.Add("Cargo");
            comboBox.Visible = false;
            labelClass.Visible = false;
            textBoxModel.Text = plane.GetModel();
            textBoxMaxHight.Text = plane.GetMaxHight().ToString();
            textBoxCruisingSpeed.Text = plane.GetCruisingSpeed().ToString();
            textBoxMaxDistance.Text = plane.GetMaxDistance().ToString();
            textBoxWeight.Text = plane.GetWeight().ToString();
            textBoxPassangerCapacity.Text = plane.GetPassengerCapacity().ToString();
            textBoxSpetialField.Text = plane.GetSpetialField().ToString();
            if (plane.GetPassengerCapacity() == 0)
            {
                comboBox.Text = "Cargo";
                textBoxPassangerCapacity.Enabled = false;
            }
            else
            {
                comboBox.Text = "Passanger";
            }
        }
        private void OkButton_Click(object sender, EventArgs e)
        {
            if (CheckCorrectData() == false)
            {
                MessageBox.Show("Please, enter correct data");
                return;
            }

            Plane plane = new Plane();
            PlaneCommonInfo plane1 = new PlaneCommonInfo();

            SetDataFromForm(plane);
            plane1.name = plane.GetModel();
            plane1.passangers = plane.GetPassengerCapacity();

            SetDialogResult(DialogResult.OK);

            if (comboBox.Text == "Passanger")
            {
                if (Convert.ToInt32(textBoxPassangerCapacity.Text) == 0)
                    MessageBox.Show("Please, enter correct data");
                else
                {
                    plane.SetSpetialFIeld(textBoxSpetialField.Text);
                    SetPP(plane1);
                    SetPlane(plane);
                    Close();
                }
            }
            else
            {
                if ((!Double.TryParse(textBoxSpetialField.Text, out double n)) || (Convert.ToInt32(textBoxPassangerCapacity.Text) != 0))
                    MessageBox.Show("Please, enter correct data");
                else
                {
                    plane.SetSpetialFIeld(textBoxSpetialField.Text);
                    SetPP(plane1);
                    SetPlane(plane);
                    Close();
                }
            }
        }
        private bool CheckCorrectData()
        {
            if ((textBoxModel.Text == "") || (textBoxCruisingSpeed.Text == "") || (textBoxMaxDistance.Text == "") || (textBoxMaxHight.Text == "")
                || (textBoxPassangerCapacity.Text == "") || (textBoxWeight.Text == "") || (!Double.TryParse(textBoxCruisingSpeed.Text, out double n1))
                || (!Double.TryParse(textBoxMaxDistance.Text, out double n2)) || (!Double.TryParse(textBoxPassangerCapacity.Text, out double n3))
                || (!Double.TryParse(textBoxWeight.Text, out double n4)) || (textBoxSpetialField.Text == "")|| (Convert.ToDouble(textBoxPassangerCapacity.Text) < 0)
                || (Convert.ToDouble(textBoxMaxHight.Text) <= 0) || (Convert.ToDouble(textBoxCruisingSpeed.Text) <= 0) || (Convert.ToDouble(textBoxMaxDistance.Text) <= 0) ||
                (Convert.ToDouble(textBoxWeight.Text) <= 0))
                return false;
            else
                return true;
        }
        private void SetDataFromForm(Plane plane)
        {
            plane.SetModel(textBoxModel.Text);
            plane.SetCruisingSpeed(Convert.ToInt32(textBoxCruisingSpeed.Text));
            plane.SetMaxDistance(Convert.ToInt32(textBoxMaxDistance.Text));
            plane.SetMaxHight(Convert.ToDouble(textBoxMaxHight.Text));
            plane.SetPassengerCapacity(Convert.ToInt32(textBoxPassangerCapacity.Text));
            plane.SetWeight(Convert.ToDouble(textBoxWeight.Text));
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            SetDialogResult(DialogResult.Cancel);
            Close();
        }
        private void comboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (comboBox.Text == "Cargo")
                labelSpetialField.Text = "Carrying capacity:";
            else
                labelSpetialField.Text = "Type of plane:";
        }
    }
}
