﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Авиакомпания;

namespace Lab_5
{
    public partial class MainForm : Form
    {
        Airline airline = AirlineFactory.CreateAirline();
        public MainForm()
        {
            InitializeComponent();
            PassangerCalculator Calculator = new PassangerCalculator();
            int totalAmount = Calculator.CalculatePassangers(airline);
            List<PlaneCommonInfo> newplanes = airline.GetPlanePassangers();
            TotalCapacity.Text = "Total passanger capacity: " + totalAmount;
            ShowPlanes show = new ShowPlanes();
            show.Show(airline, dataBox);
        }
        public void addObject(AddEdtForm AddEditForm)
        {
            if (AddEditForm.GetDialogResult() == DialogResult.OK)
            {
                if (AddEditForm.GetHelp() == DialogResult.OK)
                {
                    CargoPlane cargopl = AddEditForm.GetCargoPlane();
                    airline.addPlane(cargopl);
                }
                else
                {
                    PassengerPlane pasplane = AddEditForm.GetPasPlane();
                    airline.addPlane(pasplane);
                }
                PlaneCommonInfo plane1 = AddEditForm.GetPP();
                this.dataBox.Items.Add(plane1);
                airline.addPP(plane1);
                PassangerCalculator Calculator = new PassangerCalculator();
                int totalAmount = Calculator.CalculatePassangers(airline);
                TotalCapacity.Text = "Total passanger capacity: " + totalAmount;
            }
        }
        public void updateObject(AddEdtForm AddEditForm, int index)
        {
            if (AddEditForm.GetDialogResult() == DialogResult.OK)
            {
                Plane plane = AddEditForm.GetPlane();

                if (plane.GetPassengerCapacity() == 0)
                {
                    CargoPlane pl = AddEditForm.GetCargoPlane();
                    airline.EditPlane(pl, index);
                }
                else
                {
                    PassengerPlane pl = AddEditForm.GetPasPlane();
                    airline.EditPlane(pl, index);
                }
                PlaneCommonInfo plane1 = AddEditForm.GetPP();
                airline.EditPlaneCommonInfo(plane1, index);
                dataBox.Items.Clear();
                ShowPlanes show = new ShowPlanes();
                show.Show(airline, dataBox);
                PassangerCalculator Calculator = new PassangerCalculator();
                int totalAmount = Calculator.CalculatePassangers(airline);
                TotalCapacity.Text = "Total passanger capacity: " + totalAmount;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            AddEdtForm form2 = new AddEdtForm();
            form2.ShowDialog();
            addObject(form2);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (dataBox.SelectedItem == null)
                MessageBox.Show("Please, choose the object");
            else
            {
                int index = dataBox.SelectedIndex;
                List<Plane> newplanes = airline.GetPlanes();
                AddEdtForm form2 = new AddEdtForm(newplanes[index]);
                form2.ShowDialog();
                updateObject(form2, index);
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataBox.SelectedItem == null)
                MessageBox.Show("Please, choose the object");
            else
            {
                int index = dataBox.SelectedIndex;
                List<Plane> planes1 = airline.GetPlanes();
                List<PlaneCommonInfo> planes2 = airline.GetPlanePassangers();
                airline.deletePP(planes2[dataBox.SelectedIndex]);
                airline.deletePlane(planes1[dataBox.SelectedIndex]);
            }
            dataBox.Items.Clear();
            TotalCapacity.Text = null;
            PassangerCalculator Calculator = new PassangerCalculator();
            int totalAmount = Calculator.CalculatePassangers(airline);
            TotalCapacity.Text = "Total passanger capacity: " + totalAmount;
            ShowPlanes show = new ShowPlanes();
            show.Show(airline, dataBox);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
    public class AirlineFactory
    {
        public static Airline CreateAirline()
        {
            PassengerPlane PLANE1 = new PassengerPlane();
            PLANE1.SetCruisingSpeed(800);
            PLANE1.SetModel("ТУ-154");
            PLANE1.SetMaxDistance(5000);
            PLANE1.SetMaxHight(11000);
            PLANE1.SetPassengerCapacity(140);
            PLANE1.SetWeight(55300);
            PLANE1.SetTypeOfPlane("Ближнемагистральный");
            PlaneCommonInfo plane1;
            plane1.name = PLANE1.GetModel();
            plane1.passangers = PLANE1.GetPassengerCapacity();

            CargoPlane PLANE2 = new CargoPlane();
            PLANE2.SetCruisingSpeed(700);
            PLANE2.SetModel("Boeing 777");
            PLANE2.SetMaxDistance(4000);
            PLANE2.SetMaxHight(11500);
            PLANE2.SetCarryingCapacity(7000);
            PLANE2.SetWeight(75300);
            PlaneCommonInfo plane2;
            plane2.name = PLANE2.GetModel();
            plane2.passangers = PLANE2.GetPassengerCapacity();

            PassengerPlane PLANE3 = new PassengerPlane();
            PLANE3.SetCruisingSpeed(900);
            PLANE3.SetModel("Airbus А330");
            PLANE3.SetMaxDistance(7500);
            PLANE3.SetMaxHight(12500);
            PLANE3.SetPassengerCapacity(175);
            PLANE3.SetWeight(47600);
            PLANE3.SetPassengerCapacity(200);
            PLANE3.SetTypeOfPlane("Дальнемагистральный");
            PlaneCommonInfo plane3;
            plane3.name = PLANE3.GetModel();
            plane3.passangers = PLANE3.GetPassengerCapacity();

            CargoPlane PLANE4 = new CargoPlane();
            PLANE4.SetCruisingSpeed(830);
            PLANE4.SetModel("Airbus A320");
            PLANE4.SetMaxDistance(4000);
            PLANE4.SetMaxHight(12200);
            PLANE4.SetCarryingCapacity(5500);
            PLANE4.SetWeight(52700);
            PlaneCommonInfo plane4;
            plane4.name = PLANE4.GetModel();
            plane4.passangers = PLANE4.GetPassengerCapacity();

            Airline airline = new Airline();
            airline.addPlane(PLANE1);
            airline.addPlane(PLANE2);
            airline.addPlane(PLANE3);
            airline.addPlane(PLANE4);
            airline.addPP(plane1);
            airline.addPP(plane2);
            airline.addPP(plane3);
            airline.addPP(plane4);

            return airline;
        }
    }
    public class ShowPlanes
    {
        public void Show(Airline airline, ListBox dataBox)
        {
            List<PlaneCommonInfo> planes = airline.GetPlanePassangers();
            foreach (PlaneCommonInfo plane in planes)
            {
                dataBox.Items.Add(plane);
            }
        }
    }
}
