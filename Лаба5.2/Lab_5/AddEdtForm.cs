﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Авиакомпания;

namespace Lab_5
{
    public partial class AddEdtForm : Form
    {
        private Plane _plane;

        public Plane GetPlane()
        {
            Plane _plane = new Plane();
            SetDataFromForm(_plane);
            return _plane;
        }
        public void SetPlane(Plane _plane)
        {
            this._plane = _plane;
        }
        private PassengerPlane _pasplane;

        public PassengerPlane GetPasPlane()
        {
            PassengerPlane _pasplane = new PassengerPlane();
            SetDataFromForm(_pasplane);
            _pasplane.SetTypeOfPlane(textBox.Text);
            return _pasplane;
        }
        public void SetPasPlane(PassengerPlane _pasplane)
        {
            this._pasplane = _pasplane;
        }
        private CargoPlane _cargoplane;

        public CargoPlane GetCargoPlane()
        {
            CargoPlane _cargoplane = new CargoPlane();
            SetDataFromForm(_cargoplane);
            _cargoplane.SetCarryingCapacity(Convert.ToInt32(textBox.Text));
            return _cargoplane;
        }
        public void SetCargoPlane(CargoPlane _cargoplane)
        {
            this._cargoplane = _cargoplane;
        }

        private PlaneCommonInfo _pp;

        public PlaneCommonInfo GetPP()
        {
            _pp.name = textBoxModel.Text;
            _pp.passangers = Convert.ToInt32(textBoxPassangerCapacity.Text);
            return _pp;
        }
        public void SetPP(PlaneCommonInfo _pp)
        {
            this._pp = _pp;
        }

        private DialogResult _result;

        public DialogResult GetDialogResult()
        {
            return _result;
        }
        public void SetDialogResult(DialogResult _result)
        {
            this._result = _result;
        }

        private DialogResult _help;

        public DialogResult GetHelp()
        {
            return _help;
        }

        public void SetHelp(DialogResult _help)
        {
            this._help = _help;
        }

        public AddEdtForm()
        {
            InitializeComponent();
            comboBox.Items.Add("Passanger");
            comboBox.Items.Add("Cargo");
        }
        public AddEdtForm(Plane plane) : this()
        {
            comboBox.Items.Add("Passanger");
            comboBox.Items.Add("Cargo");
            comboBox.Visible = false;
            labelClass.Visible = false;
            textBoxModel.Text = plane.GetModel();
            textBoxMaxHight.Text = plane.GetMaxHight().ToString();
            textBoxCruisingSpeed.Text = plane.GetCruisingSpeed().ToString();
            textBoxMaxDistance.Text = plane.GetMaxDistance().ToString();
            textBoxWeight.Text = plane.GetWeight().ToString();
            textBoxPassangerCapacity.Text = plane.GetPassengerCapacity().ToString();
            if (plane.GetPassengerCapacity() == 0)
            {
                CargoPlane pl = plane as CargoPlane;
                textBox.Text = pl.GetCarryingCapacity().ToString();
                comboBox.Text = "Cargo";
                SetHelp(DialogResult.OK);
                textBoxPassangerCapacity.Enabled = false;
            }
            else
            {
                PassengerPlane pl = plane as PassengerPlane;
                textBox.Text = pl.GetTypeOfPlane().ToString();
                comboBox.Text = "Passanger";
                SetHelp(DialogResult.Cancel);
            }
        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            SetDialogResult(DialogResult.Cancel);
            Close();
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (CheckCorrectData() == false)
            {
                MessageBox.Show("Please, enter correct data");
                return;
            }
            if (comboBox.Text == "Passanger")
            {
                PassengerPlane plane = new PassengerPlane();
                PlaneCommonInfo plane1 = new PlaneCommonInfo();
                if ((Double.TryParse(textBox.Text, out double n)) || (Convert.ToInt32(textBoxPassangerCapacity.Text) == 0))
                    MessageBox.Show("Please, enter correct data");
                else
                {
                    SetDataFromForm(plane);
                    SetDialogResult(DialogResult.OK);

                    plane.SetTypeOfPlane(textBox.Text);
                    plane1.name = plane.GetModel();
                    plane1.passangers = plane.GetPassengerCapacity();
                    SetPP(plane1);
                    SetPasPlane(plane);
                    Close();
                }
            }
            else
                if (comboBox.Text == "Cargo")
            {
                CargoPlane plane = new CargoPlane();
                PlaneCommonInfo plane1 = new PlaneCommonInfo();

                if ((!Double.TryParse(textBox.Text, out double n)) || (Convert.ToInt32(textBoxPassangerCapacity.Text) != 0))
                    MessageBox.Show("Please, enter correct data");
                else
                {
                    SetDataFromForm(plane);
                    SetDialogResult(DialogResult.OK);

                    plane.SetCarryingCapacity(Convert.ToInt32(textBox.Text));
                    plane1.name = plane.GetModel();
                    plane1.passangers = plane.GetPassengerCapacity();
                    SetPP(plane1);
                    SetCargoPlane(plane);
                    Close();
                }
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox.Text == "Cargo")
            {
                label.Text = "Carrying capacity:";
            }
            else if (comboBox.Text == "Passanger")
            {
                label.Text = "Type of plane:";
            }
            else MessageBox.Show("Please, choose another type");
        }
        private bool CheckCorrectData()
        {
            if ((textBoxModel.Text == "") || (textBoxCruisingSpeed.Text == "") || (textBoxMaxDistance.Text == "") || (textBoxMaxHight.Text == "")
                || (textBoxPassangerCapacity.Text == "") || (textBoxWeight.Text == "") || (!Double.TryParse(textBoxCruisingSpeed.Text, out double n1))
                || (!Double.TryParse(textBoxMaxDistance.Text, out double n2)) || (!Double.TryParse(textBoxPassangerCapacity.Text, out double n3))
                || (!Double.TryParse(textBoxWeight.Text, out double n4)) || (textBox.Text == ""))
                return false;
            else
                return true;
        }
        private void SetDataFromForm(Plane plane)
        {
            plane.SetModel(textBoxModel.Text);
            plane.SetCruisingSpeed(Convert.ToInt32(textBoxCruisingSpeed.Text));
            plane.SetMaxDistance(Convert.ToInt32(textBoxMaxDistance.Text));
            plane.SetMaxHight(Convert.ToDouble(textBoxMaxHight.Text));
            plane.SetPassengerCapacity(Convert.ToInt32(textBoxPassangerCapacity.Text));
            plane.SetWeight(Convert.ToDouble(textBoxWeight.Text));
        }
    }
}