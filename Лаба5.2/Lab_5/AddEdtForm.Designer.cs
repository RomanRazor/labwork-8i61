﻿namespace Lab_5
{
    partial class AddEdtForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelClass = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.labelModel = new System.Windows.Forms.Label();
            this.textBoxMaxHight = new System.Windows.Forms.TextBox();
            this.labelMaxHight = new System.Windows.Forms.Label();
            this.textBoxCruisingSpeed = new System.Windows.Forms.TextBox();
            this.labelCruisingSpeed = new System.Windows.Forms.Label();
            this.textBoxMaxDistance = new System.Windows.Forms.TextBox();
            this.labelMaxDistance = new System.Windows.Forms.Label();
            this.textBoxWeight = new System.Windows.Forms.TextBox();
            this.labelWeight = new System.Windows.Forms.Label();
            this.textBoxPassangerCapacity = new System.Windows.Forms.TextBox();
            this.labelPassangerCapacity = new System.Windows.Forms.Label();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.textBox = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelClass
            // 
            this.labelClass.AutoSize = true;
            this.labelClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelClass.Location = new System.Drawing.Point(27, 21);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(45, 16);
            this.labelClass.TabIndex = 0;
            this.labelClass.Text = "Class:";
            // 
            // buttonOk
            // 
            this.buttonOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonOk.Location = new System.Drawing.Point(67, 242);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 33);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCancel.Location = new System.Drawing.Point(165, 242);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 33);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(165, 43);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(123, 20);
            this.textBoxModel.TabIndex = 6;
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelModel.Location = new System.Drawing.Point(27, 47);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(49, 16);
            this.labelModel.TabIndex = 5;
            this.labelModel.Text = "Model:";
            // 
            // textBoxMaxHight
            // 
            this.textBoxMaxHight.Location = new System.Drawing.Point(165, 69);
            this.textBoxMaxHight.Name = "textBoxMaxHight";
            this.textBoxMaxHight.Size = new System.Drawing.Size(123, 20);
            this.textBoxMaxHight.TabIndex = 8;
            // 
            // labelMaxHight
            // 
            this.labelMaxHight.AutoSize = true;
            this.labelMaxHight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMaxHight.Location = new System.Drawing.Point(27, 73);
            this.labelMaxHight.Name = "labelMaxHight";
            this.labelMaxHight.Size = new System.Drawing.Size(67, 16);
            this.labelMaxHight.TabIndex = 7;
            this.labelMaxHight.Text = "Max hight:";
            // 
            // textBoxCruisingSpeed
            // 
            this.textBoxCruisingSpeed.Location = new System.Drawing.Point(165, 95);
            this.textBoxCruisingSpeed.Name = "textBoxCruisingSpeed";
            this.textBoxCruisingSpeed.Size = new System.Drawing.Size(123, 20);
            this.textBoxCruisingSpeed.TabIndex = 10;
            // 
            // labelCruisingSpeed
            // 
            this.labelCruisingSpeed.AutoSize = true;
            this.labelCruisingSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCruisingSpeed.Location = new System.Drawing.Point(27, 99);
            this.labelCruisingSpeed.Name = "labelCruisingSpeed";
            this.labelCruisingSpeed.Size = new System.Drawing.Size(101, 16);
            this.labelCruisingSpeed.TabIndex = 9;
            this.labelCruisingSpeed.Text = "Cruising speed:";
            // 
            // textBoxMaxDistance
            // 
            this.textBoxMaxDistance.Location = new System.Drawing.Point(165, 121);
            this.textBoxMaxDistance.Name = "textBoxMaxDistance";
            this.textBoxMaxDistance.Size = new System.Drawing.Size(123, 20);
            this.textBoxMaxDistance.TabIndex = 12;
            // 
            // labelMaxDistance
            // 
            this.labelMaxDistance.AutoSize = true;
            this.labelMaxDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMaxDistance.Location = new System.Drawing.Point(27, 125);
            this.labelMaxDistance.Name = "labelMaxDistance";
            this.labelMaxDistance.Size = new System.Drawing.Size(90, 16);
            this.labelMaxDistance.TabIndex = 11;
            this.labelMaxDistance.Text = "Max distance:";
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Location = new System.Drawing.Point(165, 147);
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.Size = new System.Drawing.Size(123, 20);
            this.textBoxWeight.TabIndex = 14;
            // 
            // labelWeight
            // 
            this.labelWeight.AutoSize = true;
            this.labelWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWeight.Location = new System.Drawing.Point(27, 151);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(53, 16);
            this.labelWeight.TabIndex = 13;
            this.labelWeight.Text = "Weigth:";
            // 
            // textBoxPassangerCapacity
            // 
            this.textBoxPassangerCapacity.Location = new System.Drawing.Point(165, 173);
            this.textBoxPassangerCapacity.Name = "textBoxPassangerCapacity";
            this.textBoxPassangerCapacity.Size = new System.Drawing.Size(123, 20);
            this.textBoxPassangerCapacity.TabIndex = 16;
            // 
            // labelPassangerCapacity
            // 
            this.labelPassangerCapacity.AutoSize = true;
            this.labelPassangerCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPassangerCapacity.Location = new System.Drawing.Point(27, 177);
            this.labelPassangerCapacity.Name = "labelPassangerCapacity";
            this.labelPassangerCapacity.Size = new System.Drawing.Size(131, 16);
            this.labelPassangerCapacity.TabIndex = 15;
            this.labelPassangerCapacity.Text = "Passanger сapacity:";
            // 
            // comboBox
            // 
            this.comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Location = new System.Drawing.Point(165, 11);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(123, 21);
            this.comboBox.TabIndex = 17;
            this.comboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(165, 199);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(123, 20);
            this.textBox.TabIndex = 19;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label.Location = new System.Drawing.Point(27, 203);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(94, 16);
            this.label.TabIndex = 18;
            this.label.Text = "Type of plane:";
            // 
            // AddEdtForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 287);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.label);
            this.Controls.Add(this.comboBox);
            this.Controls.Add(this.textBoxPassangerCapacity);
            this.Controls.Add(this.labelPassangerCapacity);
            this.Controls.Add(this.textBoxWeight);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.textBoxMaxDistance);
            this.Controls.Add(this.labelMaxDistance);
            this.Controls.Add(this.textBoxCruisingSpeed);
            this.Controls.Add(this.labelCruisingSpeed);
            this.Controls.Add(this.textBoxMaxHight);
            this.Controls.Add(this.labelMaxHight);
            this.Controls.Add(this.textBoxModel);
            this.Controls.Add(this.labelModel);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelClass);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddEdtForm";
            this.Text = "AddEditForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelClass;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.TextBox textBoxMaxHight;
        private System.Windows.Forms.Label labelMaxHight;
        private System.Windows.Forms.TextBox textBoxCruisingSpeed;
        private System.Windows.Forms.Label labelCruisingSpeed;
        private System.Windows.Forms.TextBox textBoxMaxDistance;
        private System.Windows.Forms.Label labelMaxDistance;
        private System.Windows.Forms.TextBox textBoxWeight;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.TextBox textBoxPassangerCapacity;
        private System.Windows.Forms.Label labelPassangerCapacity;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Label label;
    }
}